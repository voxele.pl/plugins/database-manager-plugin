package pl.voxele.minecraft.database.mysql;

import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.plugin.Plugin;
import pl.voxele.minecraft.database.mysql.exception.SqlTemplateNotFoundException;
import pl.voxele.minecraft.database.mysql.function.MysqlConsumer;
import pl.voxele.minecraft.database.mysql.function.MysqlQueryConsumer;
import pl.voxele.minecraft.database.mysql.function.MysqlQueryFunction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

public class MysqlConnectionImpl
        implements MysqlConnection
{
    private final Executor executor = Executors.newSingleThreadExecutor();

    private final Plugin plugin;
    private final HikariDataSource dataSource;

    public MysqlConnectionImpl(Plugin plugin, HikariDataSource dataSource)
    {
        this.plugin = plugin;
        this.dataSource = dataSource;
    }

    private <T> CompletableFuture<T> async(Supplier<T> supplier)
    {
        return CompletableFuture.supplyAsync(supplier, this.executor);
    }

    @Override
    public CompletableFuture<Integer> execute(String template, MysqlConsumer consumer)
    {
        return this.async(() ->
        {
            try
            {
                String sqlTemplate = this.getSqlTemplate(template);
                try (Connection connection = this.dataSource.getConnection())
                {
                    PreparedStatement preparedStatement = connection.prepareStatement(sqlTemplate);
                    if (consumer != null)
                    {
                        consumer.accept(preparedStatement);
                    }

                    return preparedStatement.executeUpdate();
                }
            }
            catch (IOException | SQLException e)
            {
                throw new CompletionException(e);
            }
        });
    }

    @Override
    public CompletableFuture<Integer> executeGeneratedKeys(String template, MysqlConsumer consumer,
            MysqlQueryConsumer keysConsumer)
    {
        return this.async(() ->
        {
            try
            {
                String sqlTemplate = this.getSqlTemplate(template);
                try (Connection connection = this.dataSource.getConnection())
                {
                    PreparedStatement preparedStatement = connection.prepareStatement(sqlTemplate, Statement.RETURN_GENERATED_KEYS);
                    if (consumer != null)
                    {
                        consumer.accept(preparedStatement);
                    }
                    int rows = preparedStatement.executeUpdate();
                    keysConsumer.accept(preparedStatement.getGeneratedKeys());
                    return rows;
                }
            }
            catch (IOException | SQLException e)
            {
                throw new CompletionException(e);
            }
        });
    }

    @Override
    public CompletableFuture<Integer> executeCall(String template)
    {
        return this.async(() ->
        {
            try
            {
                String sqlTemplate = this.getSqlTemplate(template);
                try (Connection connection = this.dataSource.getConnection())
                {
                    return connection.prepareCall(sqlTemplate).executeUpdate();
                }
            }
            catch (SQLException | IOException e)
            {
                throw new CompletionException(e);
            }
        });
    }

    @Override
    public <T> CompletableFuture<T> executeQuery(String template, MysqlConsumer consumer, MysqlQueryFunction<T> function)
    {
        return this.async(() ->
        {
            try
            {
                String sqlTemplate = this.getSqlTemplate(template);
                try (Connection connection = this.dataSource.getConnection())
                {
                    PreparedStatement preparedStatement = connection.prepareStatement(sqlTemplate);
                    if (consumer != null)
                    {
                        consumer.accept(preparedStatement);
                    }

                    return function.apply(preparedStatement.executeQuery());
                }
            }
            catch (IOException | SQLException e)
            {
                throw new CompletionException(e);
            }
        });
    }

    @Override
    public <T> CompletableFuture<List<T>> executeQueryList(String template, MysqlConsumer consumer,
            MysqlQueryFunction<T> function)
    {
        return this.executeQuery(template, consumer, resultSet ->
        {
            List<T> list = new ArrayList<>();

            while (resultSet.next())
            {
                list.add(function.apply(resultSet));
            }

            return list;
        });
    }

    private String getSqlTemplate(String name) throws IOException
    {
        InputStream resource = this.plugin.getResource("sql/" + name + ".sql");
        if (resource == null)
        {
            throw new SqlTemplateNotFoundException("sql/" + name + ".sql");
        }
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource)))
        {
            StringBuilder text = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null)
            {
                text.append(line).append(System.lineSeparator());
            }
            return text.toString();
        }
    }
}
