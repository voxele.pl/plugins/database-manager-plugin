package pl.voxele.minecraft.database;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import pl.voxele.minecraft.configuration.PluginConfiguration;
import pl.voxele.minecraft.database.mysql.MysqlManager;
import pl.voxele.minecraft.database.mysql.MysqlManagerImpl;
import pl.voxele.minecraft.database.redis.RedisManager;
import pl.voxele.minecraft.database.redis.RedisManagerImpl;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;

public class DatabaseManagerPlugin
        extends JavaPlugin
        implements DatabaseManager
{
    private MysqlManagerImpl mysqlManager;
    private RedisManagerImpl redisManager;

    @Override
    public void onLoad()
    {
    }

    @Override
    public void onEnable()
    {
        this.saveConfig();
        this.reloadConfig();

        FileConfiguration configuration = this.getConfig();

        Map<String, HikariDataSource> mysqlConnections = this.getMysqlConnections(configuration);
        if (!mysqlConnections.containsKey("default"))
        {
            this.getLogger().warning("You not specify default mysql connection, it might be misconfigured.");
        }
        Map<String, JedisPool> redisConnections = this.getRedisConnections(configuration);
        if (!redisConnections.containsKey("default"))
        {
            this.getLogger().warning("You not specify default redis connection, it might be misconfigured.");
        }
        this.mysqlManager = new MysqlManagerImpl(mysqlConnections);
        this.redisManager = new RedisManagerImpl(redisConnections);

        Map<String, PluginConfiguration> configurations = this.getPluginConfigurations(configuration);
        configurations.forEach((pluginName, pluginConfiguration) ->
        {
            String mysqlConnection = pluginConfiguration.getMysqlConnection();
            if (mysqlConnection != null)
            {
                HikariDataSource dataSource = this.mysqlManager.getDataSource(mysqlConnection);
                if (dataSource == null)
                {
                    this.getLogger()
                            .warning(
                                    "Can not assign plugin `" + pluginName + "` to mysql connection `" + mysqlConnection + "` because not exist");
                    return;
                }
                this.mysqlManager.addPluginMapping(pluginName, mysqlConnection);
            }
            String redisConnection = pluginConfiguration.getRedisConnection();
            if (redisConnection != null)
            {
                JedisPool dataSource = this.redisManager.getRedisPool(redisConnection);
                if (dataSource == null)
                {
                    this.getLogger()
                            .warning(
                                    "Can not assign plugin `" + pluginName + "` to redis connection `" + redisConnection + "` because not exist");
                    return;
                }
                this.redisManager.addPluginMapping(pluginName, redisConnection);
            }
        });

        if (this.mysqlManager.getDataSource("default") == null)
        {
            this.getLogger().log(Level.SEVERE, "Can not find connection with `default` name");
            this.getPluginLoader().disablePlugin(this);
        }
    }

    @Override
    public void onDisable()
    {
        if (this.mysqlManager != null)
        {
            this.mysqlManager.close();
        }
        if (this.redisManager != null)
        {
            this.redisManager.close();
        }
    }

    private Map<String, HikariDataSource> getMysqlConnections(FileConfiguration configuration)
    {
        Map<String, HikariDataSource> connectionMap = new HashMap<>();

        ConfigurationSection connectionsSection = configuration.getConfigurationSection("connections.mysql");
        if (connectionsSection == null)
        {
            return connectionMap;
        }
        connectionsSection.getKeys(false).forEach(connectionName ->
        {
            ConfigurationSection connectionSection = connectionsSection.getConfigurationSection(connectionName);
            if (connectionSection == null)
            {
                return;
            }
            Properties properties = new Properties();
            connectionSection.getKeys(true).forEach(propertyName ->
            {
                properties.put(propertyName, connectionSection.get(propertyName));
            });
            HikariConfig config = new HikariConfig(properties);

            HikariDataSource connection = new HikariDataSource(config);
            connectionMap.put(connectionName.toLowerCase(Locale.ROOT), connection);
        });
        return connectionMap;
    }

    private Map<String, JedisPool> getRedisConnections(FileConfiguration configuration)
    {
        Map<String, JedisPool> connectionMap = new HashMap<>();

        ConfigurationSection connectionsSection = configuration.getConfigurationSection("connections.redis");
        if (connectionsSection == null)
        {
            return connectionMap;
        }
        connectionsSection.getKeys(false).forEach(connectionName ->
        {
            ConfigurationSection connectionSection = connectionsSection.getConfigurationSection(connectionName);
            if (connectionSection == null)
            {
                return;
            }
            String url = connectionSection.getString("url");
            if (url == null)
            {
                return;
            }
            JedisPool pool = new JedisPool(url);

            connectionMap.put(connectionName.toLowerCase(Locale.ROOT), pool);
        });
        return connectionMap;
    }

    private Map<String, PluginConfiguration> getPluginConfigurations(FileConfiguration configuration)
    {
        Map<String, PluginConfiguration> configurationMap = new HashMap<>();

        ConfigurationSection configurationSection = configuration.getConfigurationSection("plugins");
        if (configurationSection == null)
        {
            return configurationMap;
        }
        configurationSection.getKeys(false).forEach(pluginName ->
        {
            ConfigurationSection pluginSection = configurationSection.getConfigurationSection(pluginName);
            if (pluginSection == null)
            {
                return;
            }
            PluginConfiguration pluginConfiguration = new PluginConfiguration();
            pluginConfiguration.setMysqlConnection(pluginSection.getString("mysql"));
            pluginConfiguration.setRedisConnection(pluginSection.getString("redis"));

            configurationMap.put(pluginName, pluginConfiguration);
        });
        return configurationMap;
    }

    @Override
    public MysqlManager getMysqlManager()
    {
        return mysqlManager;
    }

    @Override
    public RedisManager getRedisManager()
    {
        return redisManager;
    }
}
