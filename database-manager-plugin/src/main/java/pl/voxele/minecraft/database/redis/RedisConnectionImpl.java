package pl.voxele.minecraft.database.redis;

import pl.voxele.minecraft.database.redis.function.JedisConsumer;
import pl.voxele.minecraft.database.redis.function.JedisFunction;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class RedisConnectionImpl
        implements RedisConnection
{
    private final Executor executor = Executors.newSingleThreadExecutor();

    private final JedisPool pool;

    public RedisConnectionImpl(JedisPool pool)
    {
        this.pool = pool;
    }

    @Override
    public CompletableFuture<Void> execute(JedisConsumer consumer)
    {
        return CompletableFuture.runAsync(() ->
        {
            try (Jedis jedis = this.pool.getResource())
            {
                consumer.accept(jedis);
            }
        }, this.executor);
    }

    @Override
    public <T> CompletableFuture<T> executeQuery(JedisFunction<T> function)
    {
        return CompletableFuture.supplyAsync(() ->
        {
            try (Jedis jedis = this.pool.getResource())
            {
                return function.apply(jedis);
            }
        }, this.executor);
    }

    @Override
    public Jedis getResource()
    {
        return this.pool.getResource();
    }
}
