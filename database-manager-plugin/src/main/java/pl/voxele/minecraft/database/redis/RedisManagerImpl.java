package pl.voxele.minecraft.database.redis;

import org.bukkit.plugin.Plugin;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class RedisManagerImpl
        implements RedisManager
{
    private final Map<String, String> pluginMapping = new HashMap<>();
    private final Map<String, JedisPool> connectionMap;

    public RedisManagerImpl(Map<String, JedisPool> connectionMap)
    {
        this.connectionMap = connectionMap;
    }

    public void addRedisPool(String name, JedisPool connection)
    {
        this.connectionMap.put(name.toLowerCase(Locale.ROOT), connection);
    }

    public void addPluginMapping(String pluginName, String connectionName)
    {
        this.pluginMapping.put(pluginName, connectionName);
    }

    @Override
    public RedisConnection getConnection(Plugin plugin)
    {
        String connectionName = this.pluginMapping.getOrDefault(plugin.getName(), "default");
        return new RedisConnectionImpl(this.getRedisPool(connectionName));
    }

    public JedisPool getRedisPool(String connectionName)
    {
        return this.connectionMap.get(connectionName.toLowerCase(Locale.ROOT));
    }

    public void close()
    {
        this.connectionMap.values().forEach(JedisPool::close);
    }
}
