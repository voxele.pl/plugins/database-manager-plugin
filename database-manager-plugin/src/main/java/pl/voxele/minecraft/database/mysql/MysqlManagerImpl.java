package pl.voxele.minecraft.database.mysql;

import com.zaxxer.hikari.HikariDataSource;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MysqlManagerImpl
        implements MysqlManager
{
    private final Map<String, String> pluginMapping = new HashMap<>();
    private final Map<String, HikariDataSource> connectionMap;

    public MysqlManagerImpl(Map<String, HikariDataSource> connectionMap)
    {
        this.connectionMap = connectionMap;
    }

    public void addDataSource(String name, HikariDataSource connection)
    {
        this.connectionMap.put(name.toLowerCase(Locale.ROOT), connection);
    }

    public void addPluginMapping(String pluginName, String connectionName)
    {
        this.pluginMapping.put(pluginName, connectionName);
    }

    @Override
    public MysqlConnection getConnection(Plugin plugin)
    {
        String connectionName = this.pluginMapping.getOrDefault(plugin.getName(), "default");
        return new MysqlConnectionImpl(plugin, this.getDataSource(connectionName));
    }

    public HikariDataSource getDataSource(String connectionName)
    {
        return this.connectionMap.get(connectionName.toLowerCase(Locale.ROOT));
    }

    public void close()
    {
        this.connectionMap.values().forEach(HikariDataSource::close);
    }
}
