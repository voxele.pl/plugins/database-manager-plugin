package pl.voxele.minecraft.configuration;

public class PluginConfiguration
{
    private String mysqlConnection;
    private String redisConnection;

    public String getMysqlConnection()
    {
        return mysqlConnection;
    }

    public void setMysqlConnection(String mysqlConnection)
    {
        this.mysqlConnection = mysqlConnection;
    }

    public String getRedisConnection()
    {
        return redisConnection;
    }

    public void setRedisConnection(String redisConnection)
    {
        this.redisConnection = redisConnection;
    }
}
