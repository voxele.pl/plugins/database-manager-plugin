package pl.voxele.minecraft.database.mysql;

import pl.voxele.minecraft.database.mysql.function.MysqlConsumer;
import pl.voxele.minecraft.database.mysql.function.MysqlQueryConsumer;
import pl.voxele.minecraft.database.mysql.function.MysqlQueryFunction;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface MysqlConnection
{
    CompletableFuture<Integer> execute(String template, MysqlConsumer consumer);

    CompletableFuture<Integer> executeGeneratedKeys(String template, MysqlConsumer consumer,
            MysqlQueryConsumer keysConsumer);

    CompletableFuture<Integer> executeCall(String template);

    <T> CompletableFuture<T> executeQuery(String template, MysqlConsumer consumer, MysqlQueryFunction<T> function);

    <T> CompletableFuture<List<T>> executeQueryList(String template, MysqlConsumer consumer,
            MysqlQueryFunction<T> function);
}
