package pl.voxele.minecraft.database.mysql.exception;

import java.io.IOException;

public class MysqlManagerException
        extends IOException
{
    public MysqlManagerException()
    {
    }

    public MysqlManagerException(String s)
    {
        super(s);
    }

    public MysqlManagerException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    public MysqlManagerException(Throwable throwable)
    {
        super(throwable);
    }
}
