package pl.voxele.minecraft.database.redis;

import org.bukkit.plugin.Plugin;

public interface RedisManager
{
    RedisConnection getConnection(Plugin plugin);
}
