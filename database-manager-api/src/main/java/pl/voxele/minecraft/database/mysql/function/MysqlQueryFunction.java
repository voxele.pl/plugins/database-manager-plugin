package pl.voxele.minecraft.database.mysql.function;

import java.sql.ResultSet;
import java.sql.SQLException;

@FunctionalInterface
public interface MysqlQueryFunction<R>
{
    R apply(ResultSet resultSet) throws SQLException;
}
