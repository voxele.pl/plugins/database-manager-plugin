package pl.voxele.minecraft.database.redis.function;

import redis.clients.jedis.Jedis;

@FunctionalInterface
public interface JedisFunction<R>
{
    R apply(Jedis jedis);
}
