package pl.voxele.minecraft.database.mysql.function;

import java.sql.PreparedStatement;
import java.sql.SQLException;

@FunctionalInterface
public interface MysqlConsumer
{
    void accept(PreparedStatement statement) throws SQLException;
}

