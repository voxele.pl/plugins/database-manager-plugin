package pl.voxele.minecraft.database.mysql;

import org.bukkit.plugin.Plugin;

public interface MysqlManager
{
    MysqlConnection getConnection(Plugin plugin);
}
