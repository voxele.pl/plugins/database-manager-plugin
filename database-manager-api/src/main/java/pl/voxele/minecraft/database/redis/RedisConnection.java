package pl.voxele.minecraft.database.redis;

import pl.voxele.minecraft.database.redis.function.JedisConsumer;
import pl.voxele.minecraft.database.redis.function.JedisFunction;
import redis.clients.jedis.Jedis;

import java.util.concurrent.CompletableFuture;

public interface RedisConnection
{
    CompletableFuture<Void> execute(JedisConsumer consumer);

    <T> CompletableFuture<T> executeQuery(JedisFunction<T> function);

    Jedis getResource();
}
