package pl.voxele.minecraft.database;

import pl.voxele.minecraft.database.mysql.MysqlManager;
import pl.voxele.minecraft.database.redis.RedisManager;

public interface DatabaseManager
{
    MysqlManager getMysqlManager();

    RedisManager getRedisManager();
}
