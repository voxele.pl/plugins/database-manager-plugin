package pl.voxele.minecraft.database.redis.function;

import redis.clients.jedis.Jedis;

@FunctionalInterface
public interface JedisConsumer
{
    void accept(Jedis jedis);
}
