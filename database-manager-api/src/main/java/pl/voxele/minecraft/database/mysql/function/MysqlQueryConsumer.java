package pl.voxele.minecraft.database.mysql.function;

import java.sql.ResultSet;
import java.sql.SQLException;

@FunctionalInterface
public interface MysqlQueryConsumer
{
    void accept(ResultSet resultSet) throws SQLException;
}
