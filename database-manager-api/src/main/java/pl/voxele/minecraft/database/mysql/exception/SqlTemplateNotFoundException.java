package pl.voxele.minecraft.database.mysql.exception;

public class SqlTemplateNotFoundException
        extends MysqlManagerException
{
    public SqlTemplateNotFoundException()
    {
    }

    public SqlTemplateNotFoundException(String s)
    {
        super(s);
    }

    public SqlTemplateNotFoundException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    public SqlTemplateNotFoundException(Throwable throwable)
    {
        super(throwable);
    }
}
